# IMAP Utils

## Usage

Most tools have the same generic arguments :

- `-username` - The username to use for login. (required)
- `-password` - The password to use for login. (required)
- `-server` - The imap server, defaults to localhost.
- `-debug` - Dump all the imap dialogue.

Then there are the filtering arguments.
A filter is comprised of two required elements, passed verbatim to the [imap LIST](https://www.rfc-editor.org/rfc/rfc3501#section-6.3.8) command, reference and name.
Then there are two optional element, to exclude mailboxes.
The first, exclude is used verbatim to exclude mailboxes from the list.
The second, exclude-re is compiled to a regexp and then used to exclude mailboxes from the list.

- `-filters` - Add a filter on the command line, the format is either reference:name or reference:name:exclude\[:exclude...\]
- `-filter-file` - Add filters from a file, the format is :

```toml
[[filters]]
  reference = ""
  name = "*"

[[filters]]
  reference = "foo"
  name = "*"
  exclude = ["bar"]

[[filters]]
  reference = ""
  name = "*"
  exclude-re = ["foo.*bar", "bam"]
```

Some tools like clean have an `extra` parameter added globally, and/or to each filter, description is provided when this happens.
If a filter does not have the `extra` parameter set, the global one gets used instead.
For example if you have these filters with extras:

```toml
extra = "foo"

[[filters]]
  reference = ""
  name = "*"
  extra = "bar"

[[filters]]
  reference = "foo"
  name = "*"

[[filters]]
  reference = "bar"
  name = "*"
```

They would end up as if this had been written as:

```toml
[[filters]]
  reference = ""
  name = "*"
  extra = "bar"

[[filters]]
  reference = "foo"
  name = "*"
  extra = "foo"

[[filters]]
  reference = "bar"
  name = "*"
  extra = "foo"
```

### Example usage

```shell
finddup -username bob@example.com -password super-password
```

An optional `-server xxx` can be passed if the server is not on localhost.
Another optional `-debug` can be passed to dump all the imap dialogue.

## Tools

### list

This tool will list the mailboxes that would be processed by other tools.
It helps you figure out your filters without actually doing anything.

#### Installation

```shell
go install gitlab.com/mat813/imap-tools/cmd/list@latest
```

### finddup

This tool will go over all the mailboxes from an imap server, find messages with duplicate message ids, and remove duplicates.

#### Installation

```shell
go install gitlab.com/mat813/imap-tools/cmd/finddup@latest
```

### clean

This tool will go over the mailboxes and cleanup old messages according to simples rules.

The rules are added to a filter using the extra parameter.
For example:

```toml
[[filters]]
reference = ""
name = "*"

  [filters.extra]
  0MB = 105
  5MB = 85
  10MB = 55
```

The reference and name are the same as for other tools. `extra` is a map of mailbox size in MB to days of messages that should be kept.
In this example, messages are kept up to 105 days, unless the mailbox is larger than 5MB, and oldest message is newer than 105 days, then messages are kept up to 85 days, unless the mailbox is more than 10MB and the oldest message is less than 55 days old.

#### Installation

```shell
go install gitlab.com/mat813/imap-tools/cmd/clean@latest
```
