package client

import iFilter "gitlab.com/mat813/imap-tools/internal/filter"

func (c *MyClient) AddFilter(filters ...*iFilter.Filter) {
	c.Filters = append(c.Filters, filters...)
}

func (c *MyClient) AddFilters(filters iFilter.Filters) {
	c.Filters = append(c.Filters, filters...)
}
