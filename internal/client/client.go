package client

import (
	"errors"
	"fmt"
	"log"
	"os"
	"slices"
	"sort"

	"github.com/emersion/go-imap/v2"
	"github.com/emersion/go-imap/v2/imapclient"
	iFilter "gitlab.com/mat813/imap-tools/internal/filter"
)

type MyClient struct {
	Client  *imapclient.Client
	Filters iFilter.Filters
}

var errLogout = errors.New("logout")

func NewClient(server, username, password string, debug bool, filters iFilter.Filters) *MyClient {
	client := new(MyClient)

	options := new(imapclient.Options)

	if debug {
		options.DebugWriter = log.New(os.Stdout, "", 0).Writer()
	}

	var err error

	client.Filters = make([]*iFilter.Filter, 0)

	client.Client, err = imapclient.DialStartTLS(server+":143", options)
	if err != nil {
		log.Fatalf("connect: %v", err)
	}

	if err := client.Client.Login(username, password).Wait(); err != nil {
		log.Fatalf("login: %v", err)
	}

	if len(filters) == 0 {
		filter, _ := iFilter.NewFilter("", "*", nil, nil, nil)
		client.AddFilter(&filter)
	} else {
		client.AddFilters(filters)
	}

	return client
}

func (c *MyClient) Close() error {
	return fmt.Errorf("%w: %w", errLogout, c.Client.Logout().Wait())
}

type ListData struct {
	Mailbox *imap.ListData
	Filter  iFilter.Filter
}

func (c *MyClient) GetList() []ListData {
	mailboxes := make([]ListData, 0)

	for _, filter := range c.Filters {
		mbxs, err := c.Client.List(filter.Reference, filter.Name, nil).Collect()
		if err != nil {
			log.Fatalf("list: %v", err)
		}

		for _, mbx := range mbxs {
			// Skip unselectable mailboxes (directories)
			if slices.Contains(mbx.Attrs, imap.MailboxAttrNoSelect) {
				continue
			}

			if filter.InternalRe != nil && filter.InternalRe.MatchString(mbx.Mailbox) {
				continue
			}

			mailboxes = append(mailboxes, ListData{Mailbox: mbx, Filter: *filter})
		}
	}

	// Sort by mailbox name
	sort.Slice(mailboxes, func(i, j int) bool {
		return mailboxes[i].Mailbox.Mailbox < mailboxes[j].Mailbox.Mailbox
	})

	return mailboxes
}
