package filter

import (
	"errors"
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/BurntSushi/toml"
)

type Filters []*Filter

var (
	errInvalidFilter          = errors.New("invalid filter")
	errInvalidFilterFile      = errors.New("invalid filter file")
	errProcessFiltersFromFile = errors.New("error opening file")
)

func (f *Filters) ProcessArgsFilters() func(newVal string) error {
	return func(newVal string) error {
		splitted := strings.Split(newVal, ":")
		if args := len(splitted); args == 2 { //nolint:mnd
			filter, err := NewFilter(splitted[0], splitted[1], []string{}, []string{}, nil)
			if err != nil {
				return err
			}

			*f = append(*f, &filter)
		} else if args >= 3 { //nolint:mnd
			filter, err := NewFilter(splitted[0], splitted[1], splitted[2:], []string{}, nil)
			if err != nil {
				return err
			}

			*f = append(*f, &filter)
		} else {
			return fmt.Errorf("%w: %s", errInvalidFilter, newVal)
		}

		return nil
	}
}

func (f *Filters) ProcessFiltersFromFile(path string) error {
	file, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("%w: %w", errProcessFiltersFromFile, err)
	}
	defer file.Close()

	return f.ParseTOML(file)
}

func (f *Filters) SetInternalRes() error {
	for _, filter := range *f {
		err := filter.SetInternalRe()
		if err != nil {
			return err
		}
	}

	return nil
}

func (f *Filters) SetDefaultExtra(extra any) {
	for _, filter := range *f {
		if filter.Extra == nil {
			filter.SetExtra(extra)
		}
	}
}

type TomlFilters struct {
	Extra   *any    `toml:"extra,omitempty"`
	Filters Filters `toml:"filters"`
}

func (f *Filters) ParseTOML(file io.Reader) error {
	var filters TomlFilters

	if _, err := toml.NewDecoder(file).Decode(&filters); err != nil {
		return fmt.Errorf("%w: %w", errInvalidFilterFile, err)
	}

	*f = append(*f, filters.Filters...)

	for idx := range *f {
		if (*f)[idx].Extra == nil && filters.Extra != nil {
			(*f)[idx].Extra = *filters.Extra
		}
	}

	return nil
}
