package filter

import (
	"errors"
	"fmt"
	"regexp"
)

type Filter struct {
	Reference  string         `toml:"reference"`
	Name       string         `toml:"name"`
	Exclude    []string       `toml:"exclude,omitempty"`
	ExcludeRe  []string       `mapstructure:"exclude-re" toml:"exclude-re,omitempty"`
	InternalRe *regexp.Regexp `toml:"-"` // On ignore ce champ là`
	Extra      any            `toml:"extra"`
}

var (
	errSetInternalRe    = errors.New("set internal re")
	errNewFilter        = errors.New("new filter")
	errFilterUnmarshall = errors.New("Filter unmarshal error")
)

func (f *Filter) SetInternalRe() error {
	internal := ""

	if len(f.Exclude) > 0 {
		for idx, excl := range f.Exclude {
			if idx != 0 {
				internal += "|"
			}

			internal += regexp.QuoteMeta(excl)
		}
	}

	if len(f.ExcludeRe) > 0 {
		for idx, exclRe := range f.ExcludeRe {
			if idx != 0 || len(internal) != 0 {
				internal += "|"
			}

			internal += "(?:" + exclRe + ")"
		}
	}

	if len(internal) > 0 {
		re, err := regexp.Compile(internal)
		if err != nil {
			return fmt.Errorf("%w: %w", errSetInternalRe, err)
		}

		f.InternalRe = re
	}

	return nil
}

func (*Filter) unmarshallListStrings(data any) ([]string, error) {
	list, ok := data.([]any)
	if !ok {
		return nil, fmt.Errorf(" %+v", data) //nolint:err113
	}

	returnValue := make([]string, len(list))

	for idx, entry := range list {
		strEntry, ok := entry.(string)
		if !ok {
			return nil, fmt.Errorf("[%d] %+v", idx, entry) //nolint:err113
		}

		returnValue[idx] = strEntry
	}

	return returnValue, nil
}

func (f *Filter) SetExtra(extra any) {
	f.Extra = extra
}

func (f *Filter) UnmarshalTOML(p any) error {
	data, ok := p.(map[string]any) //nolint:varnamelen
	if !ok {
		return fmt.Errorf("%w, map %v", errFilterUnmarshall, p)
	}

	var (
		reference string
		name      string
		exclude   []string
		excludeRe []string
		err       error
	)

	reference, ok = data["reference"].(string)
	if !ok {
		return fmt.Errorf("%w, reference %v", errFilterUnmarshall, data["reference"])
	}

	name, ok = data["name"].(string)
	if !ok {
		return fmt.Errorf("%w, name %v", errFilterUnmarshall, data["name"])
	}

	if data["exclude"] != nil {
		exclude, err = f.unmarshallListStrings(data["exclude"])
		if err != nil {
			return fmt.Errorf("%w: exclude%w", errFilterUnmarshall, err)
		}
	}

	if data["exclude-re"] != nil {
		excludeRe, err = f.unmarshallListStrings(data["exclude-re"])
		if err != nil {
			return fmt.Errorf("%w: excludeRe%w", errFilterUnmarshall, err)
		}
	}

	*f, err = NewFilter(reference, name, exclude, excludeRe, data["extra"])
	if err != nil {
		return fmt.Errorf("%w, NewFilter: %w", errFilterUnmarshall, err)
	}

	return nil
}

func NewFilter(reference, name string, exclude []string, excludeRe []string, extra any) (Filter, error) {
	filter := Filter{Reference: reference, Name: name, Exclude: exclude, Extra: extra} //nolint:exhaustruct

	for _, excl := range excludeRe {
		_, err := regexp.Compile(excl)
		if err != nil {
			return Filter{}, fmt.Errorf("%w: %w", errNewFilter, err)
		}

		filter.ExcludeRe = append(filter.ExcludeRe, excl)
	}

	err := filter.SetInternalRe()
	if err != nil {
		return Filter{}, fmt.Errorf("%w: %w", errNewFilter, err)
	}

	return filter, nil
}
