package parsesize

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// Storing keys in lower case as it's all bytes anyway, and it helps matching
// with input from viper.
var unitMultipliers = map[string]uint64{ //nolint:gochecknoglobals
	"b":   1,
	"kb":  1e3,     //nolint:mnd // 1000
	"mb":  1e6,     //nolint:mnd // 1000000
	"gb":  1e9,     //nolint:mnd // 1000000000
	"tb":  1e12,    //nolint:mnd // 1000000000000
	"pb":  1e15,    //nolint:mnd // 1000000000000000
	"kib": 1 << 10, //nolint:mnd // 1024
	"mib": 1 << 20, //nolint:mnd // 1048576
	"gib": 1 << 30, //nolint:mnd // 1073741824
	"tib": 1 << 40, //nolint:mnd // 1099511627776
	"pib": 1 << 50, //nolint:mnd // 1125899906842624
}

var (
	errInvalidSizeFormat   = errors.New("invalid size format")
	errSizeConvertionError = errors.New("error converting size")
	errUnknownUnit         = errors.New("unknown unit")
	// Define a regex to capture the number and the unit separately.
	re = regexp.MustCompile(`^(\d+)([a-zA-Z]+)$`)
)

func Parse(sizeStr string) (uint64, error) {
	matches := re.FindStringSubmatch(sizeStr)
	if len(matches) != 3 { //nolint:mnd
		return 0, errInvalidSizeFormat
	}

	// Extract the number part
	numberPart := matches[1]
	// Extract the unit part
	unitPart := matches[2]

	// Convert the number part to an integer
	number, err := strconv.ParseInt(numberPart, 10, 64)
	if err != nil {
		return 0, fmt.Errorf("%w: %v", errSizeConvertionError, numberPart)
	}

	// Look up the unit multiplier
	multiplier, ok := unitMultipliers[strings.ToLower(unitPart)]
	if !ok {
		return 0, fmt.Errorf("%w: %v", errUnknownUnit, unitPart)
	}

	// Return the size in bytes
	return uint64(number) * multiplier, nil
}
