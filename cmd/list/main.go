package list

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/mat813/imap-tools/cmd/base"
)

var Cmd = &cobra.Command{ //nolint:exhaustruct.gochecknoglobals
	Use:   "list",
	Short: "List mailboxes as per filters",
	Long: `This command allows to list mailboxes as per filters.

It can be used to debug filters before running commands that have a destructive
effect on the mailboxes.`,
	Run: run,
}

func run(_ *cobra.Command, _ []string) {
	client := base.NewClient()
	defer client.Close()

	for _, mbox := range client.GetList() {
		if base.Config.Debug {
			fmt.Printf(" Filter %+v\n", mbox.Filter)   //nolint:forbidigo
			fmt.Printf("Mailbox %+v\n", *mbox.Mailbox) //nolint:forbidigo
		} else {
			fmt.Printf("Mailbox %s\n", mbox.Mailbox.Mailbox) //nolint:forbidigo
		}
	}
}
