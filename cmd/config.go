package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	iFilter "gitlab.com/mat813/imap-tools/internal/filter"
)

var cfgFile string //nolint:gochecknoglobals

func init() { //nolint:gochecknoinits
	cobra.OnInitialize(initConfig)

	viper.SetDefault("Filters", iFilter.Filters{})

	viper.SetEnvPrefix("IMAP")

	RootCmd.PersistentFlags().StringVarP(
		&cfgFile, "config", "c", "", "config file (default is $HOME/.imap-tools.yaml)",
	)

	RootCmd.PersistentFlags().StringP("server", "s", "localhost", "IMAP server address")
	_ = viper.BindPFlag("Server", RootCmd.PersistentFlags().Lookup("server"))

	RootCmd.PersistentFlags().StringP("username", "u", "", "IMAP username (required)")
	_ = viper.BindPFlag("Username", RootCmd.PersistentFlags().Lookup("username"))

	RootCmd.PersistentFlags().StringP("password", "p", "", "Plain IMAP password (required)")
	_ = viper.BindPFlag("Password", RootCmd.PersistentFlags().Lookup("password"))

	RootCmd.PersistentFlags().StringP("password-command", "P", "", "Command returning the IMAP password (required)")
	_ = viper.BindPFlag("PasswordCommand", RootCmd.PersistentFlags().Lookup("password-command"))

	RootCmd.PersistentFlags().BoolP("debug", "d", false, "Enable IMAP debug mode")
	_ = viper.BindPFlag("Debug", RootCmd.PersistentFlags().Lookup("debug"))

	RootCmd.PersistentFlags().BoolP("dryrun", "n", false, "Dry run mode, don't actually perform the actions")
	_ = viper.BindPFlag("DryRun", RootCmd.PersistentFlags().Lookup("dryrun"))
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".imap-tools" (without extension).
		viper.AddConfigPath(home)
		viper.AddConfigPath(".")
		viper.SetConfigType("toml")
		viper.SetConfigName(".imap-tools")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err != nil {
		fmt.Fprintf(os.Stderr, "Error file: %v\n", err)
	}
}
