package archive

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/mat813/imap-tools/cmd/base"
)

var Cmd = &cobra.Command{ //nolint:exhaustruct,gochecknoglobals
	Use:   "archive",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: run,
}

func run(_ *cobra.Command, _ []string) {
	client := base.NewClient()
	defer client.Close()

	fmt.Printf("%-42s | %5s | %4s | %11s\n", "mailbox", "msgs", "ndel", "d_del")          //nolint:forbidigo
	fmt.Println("-------------------------------------------+-------+----+-------------") //nolint:forbidigo

	for _, mbox := range client.GetList() {
		ProcessMailbox(*client, mbox, base.Config.DryRun)
	}
}
