package archive

import (
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/emersion/go-imap/v2"
	iClient "gitlab.com/mat813/imap-tools/internal/client"
)

var errInvalidExtra = errors.New("invalid extra")

type filterExtra struct {
	Days   uint
	Format string
}

func parseExtra(extraAny any) (filterExtra, error) {
	data, ok := extraAny.(map[string]any) //nolint:varnamelen
	if !ok {
		return filterExtra{}, fmt.Errorf("%w, should be a map %+v", errInvalidExtra, extraAny)
	}

	days, ok := data["days"].(uint)
	if !ok {
		return filterExtra{}, fmt.Errorf("%w, days %v", errInvalidExtra, data["days"])
	}

	format, ok := data["format"].(string)
	if !ok {
		return filterExtra{}, fmt.Errorf("%w, days %v", errInvalidExtra, data["format"])
	}

	return filterExtra{Days: days, Format: format}, nil
}

func ProcessMailbox(client iClient.MyClient, mbox iClient.ListData, dry bool) { //nolint:funlen,cyclop
	examineOptions := imap.SelectOptions{ReadOnly: true, CondStore: false}

	selectData, err := client.Client.Select(mbox.Mailbox.Mailbox, &examineOptions).Wait()
	if err != nil {
		log.Fatalf("examine: %v", err)
	}

	// Stop if there are no messages
	if selectData.NumMessages == 0 {
		return
	}

	extra, err := parseExtra(mbox.Filter.Extra)
	if err != nil {
		log.Fatalf("%v", err)
	}

	cutoff := time.Now().AddDate(0, 0, -int(extra.Days)) //nolint:gosec

	searchData, err := client.Client.Search(&imap.SearchCriteria{ //nolint:exhaustruct
		Flag:   []imap.Flag{"\\Seen", "\\Unflagged"},
		Before: cutoff,
	}, &imap.SearchOptions{ //nolint:exhaustruct
		ReturnAll: true,
	}).Wait()
	if err != nil {
		log.Fatalf("search old: %v", err)
	}

	toDelete, _ := searchData.All.(imap.SeqSet)
	toDeleteAll, _ := toDelete.Nums()

	if count := len(toDeleteAll); count > 0 { //nolint:nestif
		if !dry {
			if _, err := client.Client.Select(mbox.Mailbox.Mailbox, nil).Wait(); err != nil {
				log.Fatalf("select: %v", err)
			}

			flags := &imap.StoreFlags{
				// On met silent à true sinon STORE est très bavard, y'a un "pipe" qui
				// se rempli quelque-part, et ça bloque indéfiniment
				Silent: true,
				Op:     imap.StoreFlagsSet,
				Flags:  []imap.Flag{imap.FlagDeleted},
			}

			if err := client.Client.Store(toDelete, flags, nil).Close(); err != nil {
				log.Fatalf("mark deleted: %v", err)
			}
		}

		fmt.Printf( //nolint:forbidigo
			"%-42s | %5d | %4d | %11s\n",
			mbox.Mailbox.Mailbox,
			selectData.NumMessages,
			count,
			cutoff.Format(time.DateOnly),
		)

		if !dry {
			if err := client.Client.UnselectAndExpunge().Wait(); err != nil {
				log.Fatalf("close: %v", err)
			}
		}
	}
}
