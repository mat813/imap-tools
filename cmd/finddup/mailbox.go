package finddup

import (
	"regexp"
	"slices"

	"github.com/emersion/go-imap/v2"
	"github.com/emersion/go-imap/v2/imapclient"
)

var MessageIDRegexp = regexp.MustCompile(`^(?i)message-id:\s*(.*?)\s*$`)

func findDups(messages []*imapclient.FetchMessageBuffer) (imap.SeqSet, []string) {
	seen := make(map[string]int)
	dups := imap.SeqSetNum()

	for _, msg := range messages {
		for key, messageIDBytes := range msg.BodySection {
			if slices.Contains(key.HeaderFields, "MESSAGE-ID") {
				// if the message does not have a message-id, messageIDBytes will
				// contain [13 10] which is not a message-id, just a newline.
				// Skip it as it is not a duplicate
				if !MessageIDRegexp.Match(messageIDBytes) {
					break
				}

				messageID := string(MessageIDRegexp.ReplaceAll(messageIDBytes, []byte("$1")))

				if seen[messageID] > 0 {
					dups.AddNum(msg.SeqNum)
				}

				seen[messageID]++

				break
			}
		}
	}

	list := make([]string, 0)

	for k := range seen {
		if seen[k] > 1 {
			list = append(list, k)
		}
	}

	return dups, list
}
