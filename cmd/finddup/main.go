package finddup

import (
	"github.com/spf13/cobra"
	"gitlab.com/mat813/imap-tools/cmd/base"
)

var Cmd = &cobra.Command{ //nolint:exhaustruct,gochecknoglobals
	Use:     "find-dup",
	Aliases: []string{"findDup", "find-dups", "findDups"},
	Short:   "Remove dupplicate emails",
	Long: `This will cleanup your mailboxes of dupplicate emails.

It will search each mailbox and if a message with the same message id is found,
it will delete the dupplicates`,
	Run: run,
}

func run(_ *cobra.Command, _ []string) {
	client := base.NewClient()
	defer client.Close()

	for _, mbox := range client.GetList() {
		ProcessMailbox(*client, mbox, base.Config.DryRun)
	}
}
