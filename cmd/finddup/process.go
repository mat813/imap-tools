package finddup

import (
	"fmt"
	"log"
	"slices"

	"github.com/emersion/go-imap/v2"
	iClient "gitlab.com/mat813/imap-tools/internal/client"
)

func ProcessMailbox(client iClient.MyClient, mbox iClient.ListData, dryRun bool) { //nolint:funlen,cyclop
	if slices.Contains(mbox.Mailbox.Attrs, imap.MailboxAttrNoSelect) {
		return
	}

	examineOptions := imap.SelectOptions{ReadOnly: true, CondStore: false}

	selectData, err := client.Client.Select(mbox.Mailbox.Mailbox, &examineOptions).
		Wait()
	if err != nil {
		log.Fatalf("examine: %v", err)
	}

	// If there are 0 or 1 messages, there cannot be any duplicates
	if selectData.NumMessages <= 1 {
		return
	}

	allMessages := imap.SeqSetNum()
	allMessages.AddRange(1, selectData.NumMessages)

	fetchOptions := imap.FetchOptions{ //nolint:exhaustruct
		BodySection: []*imap.FetchItemBodySection{
			{
				Specifier:    imap.PartSpecifierHeader,
				HeaderFields: []string{"message-id"},
				Peek:         true,
			},
		},
	}

	messages, err := client.Client.Fetch(allMessages, &fetchOptions).Collect()
	if err != nil {
		log.Fatalf("fetch: %v", err)
	}

	if dups, list := findDups(messages); len(dups) > 0 { //nolint:nestif
		if dryRun {
			fmt.Printf("[%s] %d %v %v\n", mbox.Mailbox.Mailbox, len(dups), dups, list) //nolint:forbidigo
		} else {
			if _, err := client.Client.Select(mbox.Mailbox.Mailbox, nil).Wait(); err != nil {
				log.Fatalf("select: %v", err)
			}

			flags := &imap.StoreFlags{
				Op:     imap.StoreFlagsSet,
				Silent: false,
				Flags:  []imap.Flag{imap.FlagDeleted},
			}

			if err := client.Client.Store(dups, flags, nil).Close(); err != nil {
				log.Fatalf("mark deleted: %v", err)
			}

			fmt.Printf("[%s] %d\n", mbox.Mailbox.Mailbox, len(dups)) //nolint:forbidigo

			if err := client.Client.Expunge().Close(); err != nil {
				log.Fatalf("expunge: %v", err)
			}

			if err := client.Client.UnselectAndExpunge().Wait(); err != nil {
				log.Fatalf("close: %v", err)
			}
		}
	}
}
