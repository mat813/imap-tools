package base

import (
	"log"
	"os/exec"

	"github.com/davecgh/go-spew/spew"
	"github.com/spf13/viper"
	iClient "gitlab.com/mat813/imap-tools/internal/client"
	iFilter "gitlab.com/mat813/imap-tools/internal/filter"
	"mvdan.cc/sh/v3/shell"
)

func NewClient() *iClient.MyClient {
	err := viper.Unmarshal(&Config)
	if err != nil {
		log.Fatalf("unable to parse config: %v", err)
	}

	password := Config.Password

	if Config.PasswordCommand != "" {
		// Parse the command string using the shellwords parser
		cmdArgs, err := shell.Fields(Config.PasswordCommand, nil)
		if err != nil {
			log.Fatal("Unable to exec password-command", err)
		}

		cmd := exec.Command(cmdArgs[0], cmdArgs[1:]...) //nolint:gosec

		// Run the command and capture the output
		output, err := cmd.Output()
		if err != nil {
			log.Fatal(err)
		}

		password = string(output)
	}

	extra := viper.Get("Extra")

	Config.Filters.SetDefaultExtra(extra)

	if Config.Filters == nil {
		Config.Filters = iFilter.Filters{}
	} else {
		err := Config.Filters.SetInternalRes()
		if err != nil {
			log.Fatalf("bad filters: %v", err)
		}
	}

	if Config.Debug {
		spew.Dump(Config)
	}

	return iClient.NewClient(Config.Server, Config.Username, password, Config.Debug, Config.Filters)
}
