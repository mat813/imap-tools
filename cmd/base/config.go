package base

import (
	iFilter "gitlab.com/mat813/imap-tools/internal/filter"
)

type ConfigStruct struct {
	Server          string          `mapstructure:"server"`
	Username        string          `mapstructure:"username"`
	Password        string          `mapstructure:"password"`
	PasswordCommand string          `mapstructure:"password-command"`
	Debug           bool            `mapstructure:"debug"`
	DryRun          bool            `mapstructure:"dryrun"`
	Extra           any             `mapstructure:"extra,omitempty"`
	Filters         iFilter.Filters `mapstructure:"filters,omitempty"`
}

var Config ConfigStruct //nolint:gochecknoglobals
