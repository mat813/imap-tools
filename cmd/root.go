package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

// RootCmd represents the base command when called without any subcommands.
var RootCmd = &cobra.Command{ //nolint:exhaustruct,gochecknoglobals
	Use:   "imap-tools",
	Short: "A collection of tools to manipulate IMAP mailboxes",
	Long: `These commands will help you curate your IMAP mailboxes.
	
You can remove dupplicate emails, clean old emails, or archive them`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := RootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}
