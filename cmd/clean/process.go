//nolint:err113
package clean

import (
	"fmt"
	"sort"
	"time"

	"github.com/depp/bytesize"
	"github.com/emersion/go-imap/v2"
	"github.com/emersion/go-imap/v2/imapclient"
	iClient "gitlab.com/mat813/imap-tools/internal/client"
	iParseSize "gitlab.com/mat813/imap-tools/internal/parse-size"
)

type (
	filterExtra map[uint64]uint64

	Processor struct {
		Client iClient.MyClient
		Mbox   iClient.ListData
		DryRun bool
	}
)

func (p Processor) parseExtra() (*filterExtra, error) {
	extraParsed, ok := p.Mbox.Filter.Extra.(map[string]any)
	if !ok {
		return nil, fmt.Errorf("should be a map %+v", p.Mbox.Filter.Extra)
	}

	extra := new(filterExtra)
	*extra = filterExtra{}

	for keyString := range extraParsed {
		key, err := iParseSize.Parse(keyString)
		if err != nil {
			return nil, fmt.Errorf("invalid size: %w: %v", err, key)
		}

		value, ok := extraParsed[keyString].(uint64)
		if !ok {
			return nil, fmt.Errorf("invalid value, should be int %+v", extraParsed[keyString])
		}

		(*extra)[key] = value
	}

	return extra, nil
}

func (p Processor) cleanCutoff(firstDate time.Time, totalSize uint64) (time.Time, error) {
	extra, err := p.parseExtra()
	if err != nil {
		return time.Time{}, fmt.Errorf("parseExtra: %w", err)
	}

	extraKeys := make([]uint64, 0)

	for key := range *extra {
		extraKeys = append(extraKeys, key)
	}

	sort.Slice(extraKeys, func(i, j int) bool {
		return extraKeys[i] < extraKeys[j]
	})

	now := time.Now()

	day := time.Hour * -24 //nolint:mnd

	cutoff := now.Add(day * time.Duration(int64((*extra)[extraKeys[0]]))) //nolint:gosec

	for index, cur := range extraKeys {
		curExtra := (*extra)[cur]

		if index > 0 {
			prev := extraKeys[index-1]
			if firstDate.After(cutoff) && totalSize >= prev && totalSize < cur {
				cutoff = now.Add(day * time.Duration(int64(curExtra))) //nolint:gosec
			}
		}
	}

	return cutoff, nil
}

func (p Processor) getMessages() (uint64, uint32, error) {
	examineOptions := imap.SelectOptions{ReadOnly: true, CondStore: false}

	selectData, err := p.Client.Client.Select(p.Mbox.Mailbox.Mailbox, &examineOptions).Wait()
	if err != nil {
		return 0, 0, fmt.Errorf("examine: %w", err)
	}

	// Stop if there are no messages
	if selectData.NumMessages == 0 {
		return 0, 0, nil
	}

	allMessages := imap.SeqSetNum()
	allMessages.AddRange(1, selectData.NumMessages)

	messages, err := p.Client.Client.Fetch(allMessages, &imap.FetchOptions{ //nolint:exhaustruct
		RFC822Size: true,
	}).Collect()
	if err != nil {
		return 0, 0, fmt.Errorf("fetch: %w", err)
	}

	var totalSize int64

	for _, msg := range messages {
		totalSize += msg.RFC822Size
	}

	return uint64(totalSize), selectData.NumMessages, nil
}

func (p Processor) getFirstDate() (*time.Time, uint32, error) {
	sortData, err := p.Client.Client.Sort(&imapclient.SortOptions{
		SearchCriteria: &imap.SearchCriteria{Flag: []imap.Flag{"\\Seen"}}, //nolint:exhaustruct
		SortCriteria:   []imapclient.SortCriterion{{Key: "ARRIVAL"}},      //nolint:exhaustruct
	}).Wait()
	if err != nil {
		return nil, 0, fmt.Errorf("sort: %w", err)
	}

	var first uint32
	if len(sortData) > 0 {
		first = sortData[0]
	} else {
		return nil, 0, nil
	}

	firstData, err := p.Client.Client.Fetch(imap.SeqSetNum(first), &imap.FetchOptions{ //nolint:exhaustruct
		InternalDate: true,
	}).
		Collect()
	if err != nil {
		return nil, 0, fmt.Errorf("fetch date: %w", err)
	}

	firstDate := new(time.Time)

	*firstDate = firstData[0].InternalDate

	return firstDate, first, nil
}

func (p Processor) findForExpiring(cutoff time.Time) (imap.SeqSet, []uint32, error) {
	searchData, err := p.Client.Client.Search(&imap.SearchCriteria{ //nolint:exhaustruct
		Flag:   []imap.Flag{"\\Seen"},
		Before: cutoff,
	}, &imap.SearchOptions{ //nolint:exhaustruct
		ReturnAll: true,
	}).Wait()
	if err != nil {
		return nil, nil, fmt.Errorf("search old: %w", err)
	}

	toDelete, okSearchData := searchData.All.(imap.SeqSet)
	if !okSearchData {
		return nil, nil, fmt.Errorf("invalid searchData: %+v", searchData)
	}

	toDeleteAll, okNums := toDelete.Nums()
	if !okNums {
		return nil, nil, fmt.Errorf("toDelete.Nums: %+v", toDelete)
	}

	return toDelete, toDeleteAll, nil
}

func (p Processor) Cleanup(
	numMessages, first uint32,
	totalSize uint64,
	count int,
	firstDate, cutoff time.Time,
	toDelete imap.SeqSet,
) error {
	if !p.DryRun {
		if _, err := p.Client.Client.Select(p.Mbox.Mailbox.Mailbox, nil).Wait(); err != nil {
			return fmt.Errorf("select: %w", err)
		}

		flags := &imap.StoreFlags{
			// On met silent à true sinon STORE est très bavard, y'a un "pipe" qui
			// se rempli quelque-part, et ça bloque indéfiniment
			Silent: true,
			Op:     imap.StoreFlagsSet,
			Flags:  []imap.Flag{imap.FlagDeleted},
		}

		if err := p.Client.Client.Store(toDelete, flags, nil).Close(); err != nil {
			return fmt.Errorf("mark deleted: %w", err)
		}
	}

	fmt.Printf( //nolint:forbidigo
		"%-42s | %5d | %10s | %2d | %4d | %11s | %11s | %4d\n",
		p.Mbox.Mailbox.Mailbox,
		numMessages,
		bytesize.Format(totalSize),
		first,
		count,
		firstDate.Format(time.DateOnly),
		cutoff.Format(time.DateOnly),
		int(cutoff.Sub(firstDate).Hours()/24), //nolint:mnd
	)

	if !p.DryRun {
		if err := p.Client.Client.UnselectAndExpunge().Wait(); err != nil {
			return fmt.Errorf("close: %w", err)
		}
	}

	return nil
}

func (p Processor) ProcessMailbox() error { //nolint:cyclop
	totalSize, numMessages, err := p.getMessages()
	if err != nil {
		return fmt.Errorf("getMessages: %w", err)
	}

	// If no messages, nothing to do
	if numMessages == 0 {
		return nil
	}

	firstDate, first, err := p.getFirstDate()
	if err != nil {
		return fmt.Errorf("getFirstDate: %w", err)
	} else if firstDate == nil {
		// TODO: When do we get here exactly ?
		return nil
	}

	// If there are not enough messages or their size is not big enough either...
	if numMessages <= 300 || totalSize <= 1_000_000 {
		return nil
	}

	cutoff, err := p.cleanCutoff(*firstDate, totalSize)
	if err != nil {
		return fmt.Errorf("cutoff: %w", err)
	}

	toDelete, toDeleteAll, err := p.findForExpiring(cutoff)
	if err != nil {
		return fmt.Errorf("findForExpiring: %w", err)
	}

	if count := len(toDeleteAll); count > 0 {
		err := p.Cleanup(numMessages, first, totalSize, count, *firstDate, cutoff, toDelete)
		if err != nil {
			return fmt.Errorf("cleanup: %w", err)
		}
	}

	return nil
}
