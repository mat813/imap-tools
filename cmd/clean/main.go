package clean

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/mat813/imap-tools/cmd/base"
)

var Cmd = &cobra.Command{ //nolint:exhaustruct,gochecknoglobals
	Use:     "clean",
	Aliases: []string{"cleanup"},
	Short:   "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,

	Run: run,
}

func run(_ *cobra.Command, _ []string) {
	client := base.NewClient()
	defer client.Close()

	fmt.Printf("%-42s | %5s | %10s | %2s | %4s | %11s | %11s | %4s\n", "mailbox", "msgs", "size", "1", "ndel", "d_first", "d_del", "span") //nolint:lll,forbidigo
	fmt.Println("-------------------------------------------+-------+------------+----+------+-------------+-------------+------")         //nolint:lll,forbidigo

	for _, mbox := range client.GetList() {
		processor := Processor{
			Client: *client,
			Mbox:   mbox,
			DryRun: base.Config.DryRun,
		}

		err := processor.ProcessMailbox()
		if err != nil {
			log.Printf("Processing: %s: %v", mbox.Mailbox.Mailbox, err)
		}
	}
}
